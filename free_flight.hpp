//
//  free_flight.hpp
//  BulkMC
//
//  Created by Daniel Livingston on 9/21/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#ifndef free_flight_hpp
#define free_flight_hpp

#include <stdio.h>
#include <fstream>
#include <math.h>

using namespace std;

#include "readin.hpp"

int drift(double tau);
int histograms(std::string file_name);
int isotropic(int i_fix);
int isotropic_g(int i_fix);
int isotropic_f(int i_fix);
int polar_optical_angle(int i_fix);
int Coulomb_angle_BH();
int scatter_carrier();
int free_flight_scatter();
int free_flight_scatter_new();
int write_m(int nsim, int iter, double time, bool flag_write);

#endif /* free_flight_hpp */

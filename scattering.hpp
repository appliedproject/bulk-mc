//
//  scattering.hpp
//  BulkMC
//
//  Created by Daniel Livingston on 9/21/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#ifndef scattering_hpp
#define scattering_hpp

#include <stdio.h>
#include <fstream>
#include <math.h>

using namespace std;

int acoustic_rate_2(std::string out_file, std::string out_file_2);
int acoustic_rate(std::string out_file, std::string out_file_2);
int Coulomb_BH(std::string out_file);
int polar_rate(std::string out_file_1, std::string out_file_2);
int intervalley(double w0, std::string out_file_1, std::string out_file_2);

#endif /* scattering_hpp */

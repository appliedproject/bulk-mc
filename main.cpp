///////////////////////////////////////////////////////////
//          B U L K   M O N T E   C A R L O
//                G e r m a n i u m

#define mc_ver 1.2

//
///////////////////////////////////////////////////////////
//            created by daniel livingston
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Problems that need to be addressed:
//
//     ===> Revised scattering rate causes NaN bug
//     ===> Valleys do not behave properly

///////////////////////////////////////////////////////////
// Steps to improve usability:
//
//     ===> Check if output/ and dirs are there; if not,
//              create them
//     ===> Use cwd instead of pre-set


#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <string>

using namespace std;

#include "readin.hpp"
#include "scattering.hpp"
#include "sc_tables.hpp"
#include "free_flight.hpp"



int main() {
    
    std::string file_name;
    
    //     Begin header
    std::cout << "=======================================" << std::endl;
    std::cout << "==  B U L K   M O N T E   C A R L O  ==" << std::endl;
    std::cout << "==      -- G e r m a n i u m --      ==" << std::endl;
    std::cout << "==             [ v" << mc_ver << " ]              ==" << std::endl;
    std::cout << "=======================================" << std::endl;
   
lbl_sanityBegin:
    
    readin();
    
    std::cout << "\n\nScattering Table" << std::endl;
    std::cout << "=======================================";
    
    scat_table();
    
    nsim = nele;
//    n_region = 1;
    
    init();
    
    file_name = working_dir + "bulk/initial_distribution";
    histograms(file_name);
    
    double time = 0.;
    int j = 0;
    bool flag_write = 0.;
    
    if (sanity == 1) {
        std::cout << "\n\nComputing Monte Carlo, run " << sanity_cycle << " of " << sanity_max << "...\n";
    } else {
        std::cout << "\n\nComputing Monte Carlo...\n";
    }
    
    while (time <= tot_time) {
        
        j = j + 1;
        
        time = dt*float(j);
        
        free_flight_scatter();
        
        file_name = working_dir + "bulk/current_distribution";
        histograms(file_name);
        
        write_m(nsim,j,time,flag_write);
        flag_write = 1.;
    }
    
    if ((sanity == 1) && (sanity_cycle < sanity_max)) {
        sanity_cycle++;
        
        output1.close();
        output2.close();
        output3.close();
        output4.close();
        output5.close();
        
        goto lbl_sanityBegin;
    }
    
    output1.close();
    output2.close();
    output3.close();
    output4.close();
    output5.close();
    
    std::cout << "\n\nMonte Carlo completed successfully.\n";
    return 0;
}

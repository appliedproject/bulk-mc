//
//  sc_tables.hpp
//  BulkMC
//
//  Created by Daniel Livingston on 9/21/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#ifndef sc_tables_hpp
#define sc_tables_hpp

#include <stdio.h>
#include <iostream>

using namespace std;

#include "readin.hpp"
#include "scattering.hpp"

int renormalize_table();
int scat_table();
int init();

#endif /* sc_tables_hpp */

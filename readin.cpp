//
//  readin.cpp
//  BulkMC
//
//  Created by Daniel Livingston on 9/21/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

// SOURCES:
//      (i)     Ioffe (Physical Properties of Germanium -- http://www.ioffe.ru/SVA/NSM/Semicond/Ge/)
//      (ii)    Aubrey-Fortuna (Electron transport properties in high-purity Ge
//                      down to cryogenic temperatures -- Aubrey-Fortuna et. al)
//      (iii)   Dissertation (Sergey Smirnov -- http://www.iue.tuwien.ac.at/phd/smirnov/node55.html)


#include "readin.hpp"
#include <fstream>
#include <math.h>

// Change the below working directory to match yours
std::string working_dir = "/Users/daniellivingston/MC_OUTPUT/";

int sanity_cycle = 1; // Current sanity cycle
int sanity_max = 22; // How many runs?
double elec_min = 0.0;//100000.0; // Sanity Check electric field minimum
double elec_step = 50000.0; // Sanity Check step

int n_lev = carriers;
int nele = carriers;
int nsim, iter;
int iv = 0;
int i_count = 0;

int final_valleys, i_final;
double sigma, w0, coupling_constant, delta_fi;


////////    GENERAL PARAMETERS  ////////////////

// Define fundamental constants and general parameters

int iso = 1345;
double qh = q/h;
double eps_0 = 8.85419e-12;
double two_pi = 2*pi;

// Define time step and maximum simulation time

double dt = 1.e-14; // seconds
double tot_time = 100.e-12;

// Set temperaure and doping density

double tem = 300.0; // kelvin
double Vt = kb*tem/q;
double doping_density = 1.e18; // XXXX
//Instrinsic carrier density. Assuming the old one

// Set the electric field

double fx = 0;
double fy = 7.e5; // V/m
double fz = 0;


////////    MATERIAL PARAMETERS  ////////////////

// Define masses for different valleys

double rel_mass_gamma = 0.063;
double rel_mass_L = 0.170;
double rel_mass_X = 0.58;

// Define longitudinal and transverse masses
//      Source: Ioffe

double amd, amc;
double am_l = 1.59; // XXXX
double am_t = 0.0815; // XXXX

// Define GaAs-based non-parabolicity factors

double nonparabolicity_gamma = 0.62;
double nonparabolicity_L = 0.50;
double nonparabolicity_X = 0.30;

// Define Ge-based non-parabolicity factor

double nonparabolicity_factor = 0.3;

// Define valley splitting and equivalent valleys

double split_L_gamma = -0.14;//0.29; See paper
double split_X_gamma = -0.05;//0.48; See paper (Vasileska)
int eq_valleys_gamma = 1;
int eq_valleys_L = 4;
int eq_valleys_X = 3;

// Dielectric constant for Ge
//      Source: Ioffe

double eps_low = 16.2*eps_0; // XXXX
double eps_high = eps_low; // XXXX

// Define crystal density and sound velocity
//      Source: Aubrey-Fortuna

double density = 5327.; // XXXX
double sound_velocity = 5.4E3; // XXXX

// Define coupling constants - Ge (eV)
//      Source: Aubry-Fortuna

double sigma_gamma = 5.0; // For <000>?
double sigma_L = 11.0; // For <111>?
double sigma_X = 9.0; // For <100>?

double polar_en_gamma = 0.037; // XXXX
double polar_en_L = 0.037; // XXXX
double polar_en_X = 0.037; // XXXX

double phonon_gamma_L = 0.0276; // XXXX
double phonon_L_gamma = 0.0276; // XXXX
double phonon_L_X = 0.0276; // XXXX
double phonon_X_L = 0.0276; // XXXX
double phonon_X_gamma = 0.0276; // XXXX
double phonon_gamma_X = 0.0276; // XXXX
double phonon_L_L = 0.02756; // XXXX*
double phonon_X_X = 0.03704; // XXXX*

// Define force constants - Ge (eV/m)
//      Source: Aubry-Fortuna

double DefPot_gamma_L = 2.0E10; // XXXX
double DefPot_L_gamma = 2.0E10; // XXXX
double DefPot_gamma_X = 1.0E11; // XXXX
double DefPot_X_gamma = 1.0E11; // XXXX
double DefPot_L_L = 3.0E10; // XXXX*
double DefPot_X_X = 9.46E10; // XXXX
double DefPot_L_X = 4.06E10; // XXXX
double DefPot_X_L = 4.06E10; // XXXX

// Define parameters for the scattering table

double emax = 1;
double ee, fe;
double de = emax/((float)(n_lev));

// Select scattering mechanisms

bool Coulomb_scattering = 0;//1;
bool acoustic_gamma = 1;
bool acoustic_L = 1;
bool acoustic_X = 1;
bool polar_gamma = 1;
bool polar_L = 1;
bool polar_X = 1;
bool intervalley_gamma_L = 1;
bool intervalley_gamma_X = 1;
bool intervalley_L_gamma = 1;
bool intervalley_L_L = 1;
bool intervalley_L_X = 1;
bool intervalley_X_gamma = 1;
bool intervalley_X_L = 1;
bool intervalley_X_X = 1;

double Energy_debye;
double kx, ky, kz, e;
double k, kxy, kxp, kyp, kzp, kp;

// Map parameters into internal variables used in the code

double am[3+1], tm2[3+1], hm[3+1];

double smh[3+1], hhm[3+1], freq[10+1][3+1];
double af[3+1], af2[3+1], af4[3+1];
double w[10+1][3+1], tau_max[3+1], max_scatt_mech[3+1];
double flag_mech[10+1][3+1], i_valley[10+1][3+1];
double scatt_table[4000+1][10+1][3+1];
double p[20000+1][7+1], ip[20000+1], energy[20000+1];

std::string s1 = working_dir + "bulk/vx_time_averages";
std::string s2 = working_dir + "bulk/vy_time_averages";
std::string s3 = working_dir + "bulk/vz_time_averages";
std::string s4 = working_dir + "bulk/energy_time_averages";
std::string s5 = working_dir + "bulk/valley_occupation";

std::ofstream output1;
std::ofstream output2;
std::ofstream output3;
std::ofstream output4;
std::ofstream output5;

// Set/reset certain initial values //////////////////////////////

int readin() {
    
    if (sanity == 1) {
        n_lev = carriers;
        nele = carriers;
        iv = 0;
        i_count = 0;
        
        dt = 1.e-14;
        tot_time = 100.e-12;
        
        tem = 300.0;
        Vt = kb*tem/q;
        doping_density = 1.e18;
        
        fx = 0;
        fy = elec_min + sanity_cycle*elec_step;
        fz = 0;
        
        emax = 1;
        de = emax/((float)(n_lev));
        
        s1 = working_dir + "sanity/vx_time_averages_" + std::to_string(sanity_cycle);
        s2 = working_dir + "sanity/vy_time_averages_" + std::to_string(sanity_cycle);
        s3 = working_dir + "sanity/vz_time_averages_" + std::to_string(sanity_cycle);
        s4 = working_dir + "sanity/energy_time_averages_" + std::to_string(sanity_cycle);
        s5 = working_dir + "sanity/valley_occupation_" + std::to_string(sanity_cycle);
        
        output1.open(s1.c_str());
        output2.open(s2.c_str());
        output3.open(s3.c_str());
        output4.open(s4.c_str());
        output5.open(s5.c_str());
        
    } else {
        output1.open(s1.c_str());
        output2.open(s2.c_str());
        output3.open(s3.c_str());
        output4.open(s4.c_str());
        output5.open(s5.c_str());
    }
    
    int i = 0;
    
    // Constant declaration for Si basis
    
    amd = am0*pow((am_l*am_t*am_t),(1.0/3.0));
    amc = am0*3.0/(1.0/am_l + 2.0/am_t);
    
    tm2[1] = sqrt(amc/am_l);
    tm2[2] = sqrt(amc/am_t);
    tm2[3] = sqrt(amc/am_t);
    
    hm[1] = h/amc*tm2[1];
    hm[2] = h/amc*tm2[2];
    hm[3] = h/amc*tm2[3];
    
    am[1] = amc;
    am[2] = amc;
    am[3] = amc;
    af[1] = nonparabolicity_factor;
    af[2] = nonparabolicity_factor;
    af[3] = nonparabolicity_factor;
    
    af2[i] = 2.*af[i];
    af4[i] = 4.*af[i];
    
    for (i = 1; i <= 4; i++) {
        smh[i] = sqrt(2.*amc)*sqrt(q)/h;
        hhm[i] = h/amc/q*h/2.;
        af2[i] = 2.*af[i];
        af4[i] = 4.*af[i];
    }
    
    // Mass declarations for non-equivlant valleys
    
    /*am[1] = rel_mass_gamma*am0;
    am[2] = rel_mass_L*am0;
    am[3] = rel_mass_X*am0;
    af[1] = nonparabolicity_gamma;
    af[2] = nonparabolicity_L;
    af[3] = nonparabolicity_X;
    
    for (i = 1; i <= 4; i++) {
        smh[i] = sqrt(2.*am[i])*sqrt(q)/h;
        hhm[i] = h/am[i]/q*h/2.;
        af2[i] = 2.*af[i];
        af4[i] = 4.*af[i];
    }*/
    
    
    return 0;
}

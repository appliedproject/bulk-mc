//
//  scattering.cpp
//  BulkMC
//
//  Created by Daniel Livingston on 9/21/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#include "readin.hpp"
#include "scattering.hpp"

// Call the acoustic rate //////////////////////////////

// Revised acoustic rate -- buggy
int acoustic_rate_2(std::string out_file, std::string out_file_2) {
    
    // Function has been heavily altered -- refer to original source
    //      In particular, amd vs. am[i]
    
    std::ofstream outfile(out_file.c_str());
    std::ofstream outfile2(out_file_2.c_str());
    
    double c_l, p_const, acoustic, kv, qmax, sum, sum2;
    double deltaq, qvec, minv;
    
    //double fmass = pow((rel_mass_gamma*rel_mass_L*rel_mass_X), (1.0/3.0))*am0;
    
    c_l = density*sound_velocity*sound_velocity;
    p_const = sqrt(2.)*kb*tem/pi/h/c_l*(amd/h)*(sqrt(amd)/h*sqrt(q))*(qh*q)*sigma*sigma;
    
    ///////////////////////////////
    // Begin Absorption Scattering
    ////////////////////////////////
    
    i_count = i_count + 1;
    
    outfile << "energy    acoustic" << std::endl;
    outfile2 << "energy    acoustic" << std::endl;
    
    int i, j, jmax;
    
    for (i = 1; i <= n_lev; i++) {
        ee = de*float(i);
        
        kv = sqrt(ee)*sqrt((amd/h)*qh);
        qmax = 2.0*kv*(amd*sound_velocity/h/kv+1.0);
        
        sum = 0;
        jmax = 100;
        j = 1;
        
        while (j < jmax) {
            
            deltaq = qmax/jmax;
            qvec = float(j)*deltaq;
            sum2 = pow(qvec, 2.0)*deltaq/(exp(h*sound_velocity*qvec/kb/tem)-1.0);
            sum = sum + sum2;
            
            j++;
            
        }
        
        minv = 2*kv*(amd*sound_velocity/h/kv-1);
        
        acoustic = (sum*pow(6.55, 2.0)*amd*pow(qh, 2.0)/pi/density/sound_velocity/kv)-minv;
        scatt_table[i][i_count][iv] = acoustic;
        
        outfile << ee << " " << acoustic << std::endl;
        outfile2 << ee << " " << acoustic << std::endl;
    }
    
    outfile.close();
    outfile2.close();
    
    
    flag_mech[i_count][iv] = 1;
    w[i_count][iv] = 0.;
    i_valley[i_count][iv] = iv;
    
    ////////////////////////////////
    // Begin Emission Scattering
    ////////////////////////////////
    
     i_count = i_count + 1;
     
     // outfile2 << "energy    acoustic" << std::endl;
     
     for (i = 1; i <= n_lev; i++) {
     
         ee = de*float(i);
     
         kv = sqrt(ee)*sqrt(2*(am[iv]/h)*qh);
         qmax = 2*kv*(1-am[iv]*sound_velocity/h/kv);
     
         sum = 0;
         jmax = 100;
     
         j = 1;
     
         while (j < jmax) {
  
             deltaq = qmax/jmax;
             qvec = float(j)*deltaq;
             sum2 = pow(qvec, 2.0)*deltaq/(1.0-exp(-h*sound_velocity*qvec/kb/tem));
             sum = sum + sum2;
     
             j++;
    
         }
     
         acoustic = (sum*pow(6.55, 2.0)*am[iv]*pow(qh, 2.0)/pi/density/sound_velocity/kv);
         scatt_table[i][i_count][iv] = acoustic;
         
         outfile2 << ee << " " << acoustic << std::endl;
     }
     
     outfile2.close();
     
     
     flag_mech[i_count][iv] = 1;
     w[i_count][iv] = 0.;
     i_valley[i_count][iv] = iv;
    
    
    return 0;
}

// General acoustic rate
int acoustic_rate(std::string out_file, std::string out_file_2) {
    
    double c_l, p_const, acoustic;
    
    std::ofstream outfile(out_file.c_str());
    std::ofstream outfile2(out_file_2.c_str());
    
    // Calculate constant
    
    c_l = density*sound_velocity*sound_velocity;
    p_const = sqrt(2.)*kb*tem/pi/h/c_l*(am[iv]/h)*(sqrt(am[iv])/h*sqrt(q))*(qh*q)*sigma*sigma;
    
    // Create scattering table
    
    i_count = i_count + 1;
    
    outfile << "energy    acoustic" << std::endl;
    
    int i;
    for (i = 1; i<=n_lev;i++) {
        ee = de*float(i);
        fe = ee*(1.*af[iv]*ee);
        acoustic = p_const*sqrt(fe)*(1.*af2[iv]*ee);
        scatt_table[i][i_count][iv] = acoustic;
        outfile << ee << " " << acoustic << std::endl;
    }
    
    flag_mech[i_count][iv] = 1;
    w[i_count][iv] = 0.;
    i_valley[i_count][iv] = iv;
    
    outfile.close();
    outfile2.close();
    
    return 0;
}

// Coulomb Scattering //////////////////////////////

int Coulomb_BH(std::string out_file) {
    
    double Debye_length, final_mass, factor, p_const;
    double scatt_rate, ge;
    
    // Calculate constants
    Debye_length = sqrt(eps_high*Vt/q/doping_density);
    Energy_debye = hhm[iv]/Debye_length/Debye_length;
    final_mass = am[iv];
    factor = Debye_length*Debye_length/eps_high;
    p_const = doping_density*final_mass*qh*sqrt(2.*final_mass)/pi*qh*factor*qh*factor*qh*sqrt(q);
    
    // Calculate scattering rate:
    
    i_count = i_count + 1;
    
    std::ofstream outfile(out_file.c_str());
    
    outfile << "energy    coulomb" << std::endl;
    
    int i;
    for (i = 1; i <= n_lev; i++) {
        ee = de*float(i);
        ge = ee*(1.+af[iv]*ee);
        factor = sqrt(ge)*(1.+af2[iv]*ee)/(1.+4.*ge/Energy_debye);
        scatt_rate = p_const*factor;
        scatt_table[i][i_count][iv] = scatt_rate;
        outfile << ee << " " << scatt_rate << std::endl;
    }
    
    outfile.close();
    
    flag_mech[i_count][iv] = 3;
    w[i_count][iv] = 0.0;
    i_valley[i_count][iv] = iv;
    
    
    return 0;
}

// Polar Rate //////////////////////////////

int polar_rate(std::string out_file_1, std::string out_file_2) {
    std::ofstream outfile(out_file_1.c_str());
    std::ofstream outfile2(out_file_2.c_str());
    
    double rnq, p_const, polar_ab, polar_em, A, B, C;
    double ef, ge, gf, rnum, denom, absorption, emission, factor;
    
    // Calculate constant
    
    rnq = 1.0/(exp(w0/Vt)-1.0);
    p_const = qh*qh*q*w0*sqrt(am[iv]/2./q)/4./pi* (1./eps_high - 1./eps_low);
    
    // (a) Scattering rate - absorption
    
    i_count = i_count + 1;
    
    outfile << "energy     polar" << std::endl;
    polar_ab = rnq*p_const;
    
    int i;
    
    for (i = 1; i <= n_lev; i++) {
        
        ee = de*float(i);
        ef = ee + w0;
        ge = ee*(1.+af[iv]*ee);
        gf = ef*(1.+af[iv]*ef);
        rnum = sqrt(ge) + sqrt(gf);
        denom = sqrt(ge) - sqrt(gf);
        A = pow((2*(1.+af2[iv]*ee)*(1.+af[iv]*ef) + af[iv]*(ge+gf)),2);
        B = - af2[iv]*sqrt(ge*gf) * (4.*(1.+af[iv]*ee)*(1.+af[iv]*ef) + af[iv]*(ge+gf));
        C = 4.0*(1.+af[iv]*ee)*(1.+af[iv]*ef)*(1.+af2[iv]*ee)*(1.+af2[iv]*ef);
        A = 4.0;
        C = 4.0;
        B = 0.0;
        factor = (1.+af2[iv]*ef)/sqrt(ge)*(A*log(fabs(rnum/denom))+B)/C;
        absorption = polar_ab*factor;
        scatt_table[i][i_count][iv] = absorption;
        outfile << ee << " " << absorption << std::endl;
        
    }
    
    outfile.close();
    
    flag_mech[i_count][iv] = 2;
    w[i_count][iv] = w0;
    i_valley[i_count][iv] = iv;
    
    // (b) Scattering rate - emission
    
    i_count = i_count + 1;
    
    outfile2 << "energy     polar" << endl;
    
    polar_em = (1.0+rnq)*p_const;
    
    for (i = 1; i <= n_lev; i++) {
        
        ee = de*float(i);
        ef = ee - w0;
        
        if (ef <= 0) {
            emission = 0;
        }
        else {
            ge = ee*(1.+af[iv]*ee);
            gf = ef*(1.+af[iv]*ef);
            rnum = sqrt(ge) + sqrt(gf);
            denom = sqrt(ge) - sqrt(gf);
            A = pow((2*(1.+af2[iv]*ee)*(1.+af[iv]*ef)+af[iv]*(ge+gf)),2);
            B = - af2[iv]*sqrt(ge*gf)*(4.*(1.+af[iv]*ee)*(1.+af[iv]*ef)+af[iv]*(ge+gf));
            C = 4.*(1.+af[iv]*ee)*(1.+af[iv]*ef)*(1.+af2[iv]*ee)*(1.+af2[iv]*ef);
            A = 4.;
            C = 4.;
            B = 0.;
            factor = (1.+af2[iv]*ef)/sqrt(ge)*(A*log(fabs(rnum/denom))+B)/C;
            emission = polar_em*factor;
        }
        
        scatt_table[i][i_count][iv] = emission;
        outfile2 << ee << " " << emission << std::endl;
        
    }
    
    outfile2.close();
    
    flag_mech[i_count][iv] = 2;
    w[i_count][iv] = - w0;
    i_valley[i_count][iv] = iv;
    
    return 0;
}

// Intervalley //////////////////////////////

int intervalley(double w0, std::string out_file_1, std::string out_file_2) {
    std::ofstream outfile(out_file_1.c_str());
    std::ofstream outfile2(out_file_2.c_str());
    
    // Calculate constants
    
    double rnq, final_mass, p_const, ab, em, factor;
    double ef, gf, absorption, emission;
    
    rnq = 1./(exp(w0/Vt)-1.);
    final_mass = am[i_final];
    p_const = final_valleys*(pow(coupling_constant,2))*q*sqrt(q)/(sqrt(2.)*pi*density*w0)*(final_mass/h)*sqrt(final_mass)/h;
    
    // (a) Scattering rate - absorption
    
    i_count = i_count + 1;
    
    outfile << "energy    absorption" << std::endl;
    
    ab = rnq*p_const;
    
    int i;
    for (i = 1; i <= n_lev; i++) {
        
        ee = de*float(i);
        ef = ee + w0 - delta_fi;
        gf = ef*(1.+af[i_final]*ef);
        
        if (ef <= 0) {
            absorption = 0.;
        } else {
            factor = sqrt(gf)*(1.+af2[i_final]*ef);
            absorption = ab*factor;
        }
        
        scatt_table[i][i_count][iv] = absorption;
        outfile << ee << " " << absorption << std::endl;
        
    }
    
    outfile.close();
    
    flag_mech[i_count][iv] = 1;
    w[i_count][iv] = w0 - delta_fi;
    i_valley[i_count][iv] = i_final;
    
    // (b) Scattering rate - emission
    
    i_count = i_count + 1;
    
    outfile2 << "energy     emission" << std::endl;
    
    em = (1.+rnq)*p_const;
    
    for (i = 1; i <= n_lev; i++) {
        
        ee = de*float(i);
        ef = ee - w0 - delta_fi;
        gf = ef*(1.+af[i_final]*ef);
        
        if (ef <= 0) {
            emission = 0.;
        } else {
            factor = sqrt(gf)*(1.+af2[i_final]*ef);
            emission = em*factor;
        }
        
        scatt_table[i][i_count][iv] = emission;
        outfile2 << ee << " " << emission << std::endl;
        
    }
    
    outfile2.close();
    
    flag_mech[i_count][iv] = 1;
    w[i_count][iv] = - w0 - delta_fi;
    i_valley[i_count][iv] = i_final;
    
    return 0;
}
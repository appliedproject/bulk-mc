//
//  readin.hpp
//  BulkMC
//
//  Created by Daniel Livingston on 9/21/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#ifndef readin_hpp
#define readin_hpp

#define sanity 0 // Perform sanity checks?

#define am0 9.11e-31
#define h 1.05459e-34
#define q 1.60219e-19
#define eps 8.85419e-12
#define kb 1.38055e-23
#define pi 3.14159265
#define carriers 5000

#include <string>

using namespace std;

// Change the below working directory to match yours
extern std::string working_dir;

extern int sanity_cycle, sanity_max;
extern double elec_min, elec_step;

extern int n_lev, nele, nsim, iter;
extern int iv, i_count;

extern int final_valleys, i_final;
extern double sigma, w0, coupling_constant, delta_fi;

extern int iso;
extern double qh, eps_0, two_pi;

extern double dt, tot_time;
extern double tem, Vt, doping_density;
extern double fx, fy, fz;

extern double am_l, am_t, amd, amc;
extern double nonparabolicity_factor;

extern double rel_mass_gamma, rel_mass_L, rel_mass_X;
extern double nonparabolicity_gamma, nonparabolicity_L, nonparabolicity_X;
extern double split_L_gamma, split_X_gamma;
extern int eq_valleys_gamma, eq_valleys_L, eq_valleys_X;

extern double eps_high, eps_low;
extern double density, sound_velocity;
extern double emax, ee, fe, de;

extern bool Coulomb_scattering;
extern bool acoustic_gamma, acoustic_L, acoustic_X;
extern bool polar_gamma, polar_L, polar_X;
extern bool intervalley_gamma_L, intervalley_gamma_X, intervalley_L_gamma, intervalley_L_L;
extern bool intervalley_L_X, intervalley_X_gamma, intervalley_X_L, intervalley_X_X;

extern double DefPot_zero_g, DefPot_zero_f, DefPot_first_g, DefPot_first_f;
extern double sigma_acoustic, phonon_zero_g, phonon_zero_f, phonon_first_g, phonon_first_f;

extern double sigma_gamma, sigma_L, sigma_X;
extern double polar_en_gamma, polar_en_L, polar_en_X;
extern double phonon_gamma_L, phonon_gamma_X, phonon_L_gamma, phonon_L_L, phonon_L_X, phonon_X_gamma, phonon_X_L, phonon_X_X;
extern double DefPot_gamma_L, DefPot_L_gamma, DefPot_gamma_X, DefPot_X_gamma, DefPot_L_L, DefPot_X_X, DefPot_L_X, DefPot_X_L;

extern double Energy_debye;
extern double kx, ky, kz, e;
extern double k, kxy, kxp, kyp, kzp, kp;

extern double am[3+1], tm2[3+1], hm[3+1];

extern double smh[3+1], hhm[3+1], freq[10+1][3+1];
extern double af[3+1], af2[3+1], af4[3+1];
extern double w[10+1][3+1], tau_max[3+1], max_scatt_mech[3+1];
extern double flag_mech[10+1][3+1], i_valley[10+1][3+1];
extern double scatt_table[4000+1][10+1][3+1];
extern double p[20000+1][7+1], ip[20000+1], energy[20000+1];

extern std::string s1, s2, s3, s4, s5;
extern std::ofstream output1, output2, output3, output4, output5;

int readin();

#endif /* readin_hpp */

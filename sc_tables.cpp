//
//  sc_tables.cpp
//  BulkMC
//
//  Created by Daniel Livingston on 9/21/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#include "sc_tables.hpp"

// Re-normalize the table //////////////////////////////

int renormalize_table() {
    
    int i, ik, i_max;
    double tau;
    
    max_scatt_mech[iv] = i_count;
    
    if (max_scatt_mech[iv] >= 1) {
        if (max_scatt_mech[iv] > 1) {
            
            for (i = 2; i <= max_scatt_mech[iv]; i++) {
                for (ik = 1; ik <= n_lev; ik++) {
                    scatt_table[ik][i][iv] = scatt_table[ik][i-1][iv] + scatt_table[ik][i][iv];
                }
            }
            
        }
        
        
        
        // Variables may have wrong type (real -> integer)
        
        i_max = max_scatt_mech[iv];
        tau = 0.;
        
        for (i = 1; i <= n_lev; i++) {
            if (scatt_table[i][i_max][iv] > tau) {
                tau = scatt_table[i][i_max][iv];
            }
        }
        
        for (i = 1; i <= max_scatt_mech[iv]; i++) {
            for (ik = 1; ik <= n_lev; ik++) {
                scatt_table[ik][i][iv] = scatt_table[ik][i][iv]/tau;
            }
        }
        
        tau_max[iv] = 1./tau;
        
        cout << "   valley index = " << iv << "\n   tau_max = " << tau_max[iv] << "\n   tau = " << tau << " ";
    }
    
    return 0;
}

// Scattering table //////////////////////////////

int scat_table() {
    
    std::string table_r;
    
    //////////////////////////////////////////
    //          G A M M A  V A L L E Y
    //
    //////////////////////////////////////////
    
    iv = 1;
    i_count = 0;
    
    // Acoustic phonons scattering rate
    
    if (acoustic_gamma == 1) {
        sigma = sigma_gamma;
        acoustic_rate(working_dir + "gamma/acoustic_ab",working_dir + "gamma/acoustic_em");
    }
    
    // Coulomb scattering rate - Brooks-Herring approach
    
    if (Coulomb_scattering == 1) {
        Coulomb_BH(working_dir + "gamma/coulomb");
    }
    
    // Polar optical phonons scattering rate
    
    if (polar_gamma == 1) {
        w0 = polar_en_gamma;
        polar_rate(working_dir + "gamma/polar_ab",working_dir + "gamma/polar_em");
    }
    
    // Intervalley scattering: gamma to L valley
    
    if (intervalley_gamma_L == 1) {
        w0 = phonon_gamma_L;
        coupling_constant = DefPot_gamma_L;
        delta_fi = split_L_gamma;
        final_valleys = eq_valleys_L;
        i_final = 2;
        intervalley(w0,working_dir + "gamma/intervalley_gamma_L_ab",working_dir + "gamma/intervalley_gamma_L_em");
    }
    
    // Intervalley scattering: gamma to X valley
    
    if (intervalley_gamma_X == 1) {
        w0 = phonon_gamma_X;
        coupling_constant = DefPot_gamma_X;
        delta_fi = split_X_gamma;
        final_valleys = eq_valleys_X;
        i_final = 3;
        intervalley(w0,working_dir + "gamma/intervalley_gamma_X_ab",working_dir + "gamma/intervalley_gamma_X_em");
    }
    
    std::cout << "\n\nMechanisms in the gamma valley = " << i_count << "\n\n";
    renormalize_table();
    
    if (i_count > 0) {
        
        table_r = working_dir + "gamma/gamma_table_renormalized";
        std::ofstream g_file(table_r.c_str());
        
        int i;
        for (i = 1; i<=n_lev; i++) {
            ee = float(i)*de;
            
            g_file << ee << " " << scatt_table[i][1][1] << " " << scatt_table[i][2][1] << " " << scatt_table[i][3][1] << " " << scatt_table[i][4][1] << " " << scatt_table[i][5][1] << " " << scatt_table[i][6][1] << " " << scatt_table[i][7][1] << " " << scatt_table[i][8][1] << " " << scatt_table[i][9][1] << " " << scatt_table[i][10][1] << std::endl;
            
        }
        
        g_file.close();
    }
    
    //////////////////////////////////////////
    //          L  V A L L E Y
    //
    //////////////////////////////////////////
    
    
    iv = 2;
    i_count = 0;
    
    // Acoustic phonons scattering rate
    
    if (acoustic_L == 1) {
        sigma = sigma_L;
        acoustic_rate(working_dir + "L/acoustic_ab",working_dir + "L/acoustic_em");
    }
    
    // Coulomb scattering rate - Brooks-Herring approach
    
    if (Coulomb_scattering == 1) {
        Coulomb_BH(working_dir + "L/coulomb");
    }
    
    // Polar optical phonons scattering rate
    
    if (polar_L == 1) {
        w0 = polar_en_L;
        polar_rate(working_dir + "L/polar_ab",working_dir + "L/polar_em");
    }
    
    // Intervalley scattering: gamma to L valley
    
    if (intervalley_L_gamma == 1) {
        w0 = phonon_L_gamma;
        coupling_constant = DefPot_L_gamma;
        delta_fi = split_L_gamma;
        final_valleys = eq_valleys_gamma;
        i_final = 1;
        intervalley(w0,working_dir + "L/intervalley_L_gamma_ab",working_dir + "L/intervalley_L_gamma_em");
    }
    
    // Intervalley scattering: gamma to X valley
    
    if (intervalley_L_L == 1) {
        w0 = phonon_L_L;
        coupling_constant = DefPot_L_L;
        delta_fi = 0.;
        final_valleys = eq_valleys_L - 1;
        i_final = 2;
        intervalley(w0,working_dir + "L/intervalley_L_L_ab",working_dir + "L/intervalley_L_L_em");
    }
    
    
    // Intervalley scattering: gamma to X valley
    
    if (intervalley_L_X == 1) {
        w0 = phonon_L_X;
        coupling_constant = DefPot_L_X;
        delta_fi = split_X_gamma - split_L_gamma;
        final_valleys = eq_valleys_X;
        i_final = 3;
        intervalley(w0,working_dir + "L/intervalley_L_X_ab",working_dir + "L/intervalley_L_X_em");
    }
    
    std::cout << "\n\nMechanisms in the L valley = " << i_count << "\n\n";
    renormalize_table();
    
    if (i_count > 0) {
        table_r = working_dir + "L/L_table_renormalized";
        std::ofstream l_file(table_r.c_str());
        
        int i;
        for (i = 1; i<=n_lev; i++) {
            ee = float(i)*de;
            
            l_file << ee << " " << scatt_table[i][1][2] << " " << scatt_table[i][2][2] << " " << scatt_table[i][3][2] << " " << scatt_table[i][4][2] << " " << scatt_table[i][5][2] << " " << scatt_table[i][6][2] << " " << scatt_table[i][7][2] << " " << scatt_table[i][8][2] << " " << scatt_table[i][9][2] << " " << scatt_table[i][10][2] << std::endl;
            
        }
        
        l_file.close();
    }
    
    //////////////////////////////////////////
    //          X  V A L L E Y
    //
    //////////////////////////////////////////
    
    
    iv = 3;
    i_count = 0;
    
    // Acoustic phonons scattering rate
    
    if (acoustic_X == 1) {
        sigma = sigma_X;
        acoustic_rate(working_dir + "X/acoustic_ab",working_dir + "X/acoustic_em");
    }
    
    // Coulomb scattering rate - Brooks-Herring approach
    
    if (Coulomb_scattering == 1) {
        Coulomb_BH(working_dir + "X/coulomb");
    }
    
    // Polar optical phonons scattering rate
    
    if (polar_X == 1) {
        w0 = polar_en_X;
        polar_rate(working_dir + "X/polar_ab",working_dir + "X/polar_em");
    }
    
    // Intervalley scattering: gamma to L valley
    
    if (intervalley_X_gamma == 1) {
        w0 = phonon_X_gamma;
        coupling_constant = DefPot_X_gamma;
        delta_fi = - split_X_gamma;
        final_valleys = eq_valleys_gamma;
        i_final = 1;
        intervalley(w0,working_dir + "X/intervalley_X_gamma_ab",working_dir + "X/intervalley_X_gamma_em");
    }
    
    // Intervalley scattering: gamma to X valley
    
    if (intervalley_X_L == 1) {
        w0 = phonon_X_L;
        coupling_constant = DefPot_X_L;
        delta_fi = split_L_gamma - split_X_gamma;
        final_valleys = eq_valleys_L;
        i_final = 2;
        intervalley(w0,working_dir + "X/intervalley_X_L_ab",working_dir + "X/intervalley_X_L_ab");
    }
    
    
    // Intervalley scattering: gamma to X valley
    
    if (intervalley_X_X == 1) {
        w0 = phonon_X_X;
        coupling_constant = DefPot_X_X;
        delta_fi = 0.;
        final_valleys = eq_valleys_X - 1.;
        i_final = 3;
        intervalley(w0,working_dir + "X/intervalley_X_X_ab",working_dir + "X/intervalley_X_X_em");
    }
    
    std::cout << "\n\nMechanisms in the X valley = " << i_count << "\n\n";
    renormalize_table();
    
    if (i_count > 0) {
        
        table_r = working_dir + "X/X_table_renormalized";
        std::ofstream x_file(table_r.c_str());
        
        int i;
        for (i = 1; i<=n_lev; i++) {
            ee = float(i)*de;
            
            x_file << ee << " " << scatt_table[i][1][3] << " " << scatt_table[i][2][3] << " " << scatt_table[i][3][3] << " " << scatt_table[i][4][3] << " " << scatt_table[i][5][3] << " " << scatt_table[i][6][3] << " " << scatt_table[i][7][3] << " " << scatt_table[i][8][3] << " " << scatt_table[i][9][3] << " " << scatt_table[i][10][3] << std::endl;
            
        }
        
        x_file.close();
    }
    
    return 0;
}


// Init //////////////////////////////

int init() {
    
    double sume = 0.;
    double rr, ct, st, tc, fai;
    
    int i;
    for (i = 1; i <= nsim; i++) {
        //srand(iso);
        e = -(1.5*Vt)*log(rand()/(double)RAND_MAX);
        sume = sume + e;
        
        //  Initial valley index
        
        //srand(iso);
        rr = 3.0*(double)rand()/RAND_MAX;
        
        if (rr <= 1) {
            iv = 1;
        } else if (rr <= 2) {
            iv = 2;
        } else if (rr <= 3) {
            iv = 3;
        }
        
        iv = 1; // Disable for Ge?
        
        //  Initial wavevector
        
        k = smh[iv]*sqrt(e*(1.+af[iv]*e));
        fai = two_pi*(double)rand()/RAND_MAX;
        ct = 1.-2.*(double)rand()/RAND_MAX;
        st = sqrt(1.-ct*ct);
        kx = k*st*cos(fai);
        ky = k*st*sin(fai);
        kz = k*ct;
        
        //  Initial free-flight
        
        //srand(iso);
        rr = (double)rand()/RAND_MAX;
        
        while (rr <= 1.0e-5) {
            rr = (double)rand()/RAND_MAX;
        }
        
        tc = -(log(rr))*tau_max[iv];
        
        //  Map particle atributes
        
        p[i][1] = kx;
        p[i][2] = ky;
        p[i][3] = kz;
        p[i][4] = tc;
        ip[i] = iv;
        energy[i] = e;
        // cout << "energy[" << i << "] : " << energy[i] << std::endl;
        
        
    }
    
    sume = sume/float(nsim);
    std::cout << "\n\nAverage carrier energy (init_dist)";
    std::cout << "\n=======================================\n";
    std::cout << "   Energy = " << sume << "\n";
    
    return 0;
}

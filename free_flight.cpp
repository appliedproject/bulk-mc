//
//  free_flight.cpp
//  BulkMC
//
//  Created by Daniel Livingston on 9/21/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#include "free_flight.hpp"

// Drift //////////////////////////////

int drift(double tau) {
    
    double qh1, dkx, dky, dkz;
    double skx, sky, skz, sk, gk;
    
    qh1 = qh*tau;
    dkx = -qh1*fx;
    dky = -qh1*fy;
    dkz = -qh1*fz;
    
    kx = kx+dkx;
    ky = ky+dky;
    kz = kz+dkz;
    
    skx = kx*kx;
    sky = ky*ky;
    skz = kz*kz;
    sk = skx+sky+skz;
    k = sqrt(sk);
    gk = hhm[iv]*sk;
    e = 2*gk/(1.+sqrt(1.+af4[iv]*gk));
    
    return 0;
}

//  Save Histograms //////////////////////////////

int histograms(std::string file_name) {
    
    std::ofstream outfile(file_name.c_str());
    
    outfile << "kx    ky    kz    energy" << std::endl;
    
    int i;
    for (i = 1; i <= nsim; i++) {
        kx = p[i][1];
        ky = p[i][2];
        kz = p[i][3];
        e = energy[i];
        outfile << kx << " " << ky << " " << kz << " " << e << std::endl;
    }
    
    outfile.close();
    return 0;
}

// Isotropic //////////////////////////////

int isotropic(int i_fix) {
    
    double ct, rknew, fi, st;
    
    //  Update carrier energy
    e = e + w[i_fix][iv];
    
    //  Update carrier wavevector
    rknew = smh[i_final]*sqrt(e*(1.+af[i_final]*e));
    
    fi = two_pi*rand()/(double)RAND_MAX;
    ct = 1.-2.*rand()/(double)RAND_MAX;
    st = sqrt(1.-ct*ct);
    
    kx = rknew*st*cos(fi);
    ky = rknew*st*sin(fi);
    kz = rknew*ct;
    
    return 0;
}

////// ISOTROPIC_G* //////////////////////////////

int isotropic_g(int i_fix) {
    
    double rknew, ct, st, fi;
    
    e = e + w[i_fix][iv];
    //e = e + w[i_fix][i_region];
    
    //rknew = smh[iv]*sqrt(e*(1.+af[iv]*e));
    rknew = smh[i_final]*sqrt(e*(1.+af[i_final]*e));
    
    fi = two_pi*rand()/(double)RAND_MAX;
    ct = 1.-2.*rand()/(double)RAND_MAX;
    st = sqrt(1.-ct*ct);
    
    kx = rknew*st*cos(fi);
    ky = rknew*st*sin(fi);
    kz = rknew*ct;
    
    return 0;
    
}

////// ISOTROPIC_F* /////////////////////////////////

int isotropic_f(int i_fix) {
    
    double rknew, ct, st, fi;
    int iv0 = 0;
    
    e = e + w[i_fix][iv];
    //    e = e + w[i_fix][i_region];
    
    //rknew = smh[iv]*sqrt(e*(1+af[iv]*e));
    rknew = smh[i_final]*sqrt(e*(1+af[i_final]*e));
    fi = two_pi*rand()/(double)RAND_MAX;
    ct = 1.-2.*rand()/(double)RAND_MAX;
    st = sqrt(1.-ct*ct);
    
    kx = rknew*st*cos(fi);
    ky = rknew*st*sin(fi);
    kz = rknew*ct;
    
    double rr = rand()/(double)RAND_MAX;
    
    if (iv == 1) {
        
        if (rr <= 0.5) {
            iv0 = 2;
        } else {
            iv0 = 3;
        }
        
    }
    
    if (iv == 2) {
        if (rr <= 0.5) {
            iv0 = 1;
        } else {
            iv0 = 3;
        }
    }
    
    if (iv == 3) {
        if (rr <= 0.5) {
            iv0 = 1;
        } else {
            iv0 = 2;
        }
    }
    
    iv = iv0;
    
    return 0;
}

// Polar Optical Angle //////////////////////////////

int polar_optical_angle(int i_fix) {
    
    double enew, gnew, zeta, ge, rr;
    double cth0, sth0, cfi0, sfi0, cth, sth, fi, cfi, sfi;
    
    //  Update carrier energy
    enew = e + w[i_fix][iv];
    
    //  Calculate the rotation angles
    kxy = sqrt(kx*kx+ky*ky);
    k = sqrt(kxy*kxy+kz*kz);
    cth0 = kz/k;
    sth0 = kxy/k;
    cfi0 = kx/kxy;
    sfi0 = ky/kxy;
    
    //  Randomize momentum in the rotated coordinate system
    kp = smh[iv]*sqrt(enew*(1.0+af[iv]*enew));
    ge = e*(1.0+af[iv]*e);
    gnew = enew*(1.0+af[iv]*enew);
    zeta = 2.0*sqrt(ge*gnew)/(ge+gnew-2.0*sqrt(ge*gnew));
    
    rr = rand()/(double)RAND_MAX;
    
    // cth is the problem line, that eventually runs
    //     energy throught the ground, vicariously
    //     thorugh kx, ky, kz
    
    cth = ((zeta+1)-pow((2*zeta + 1),rr))/zeta;
    //  cth = 1 - rr; // Temporary approximation
    sth = sqrt(1.-cth*cth);
    
    fi = two_pi*rand()/(double)RAND_MAX;
    cfi = cos(fi);
    sfi = sin(fi);
    kxp = kp*sth*cfi;
    kyp = kp*sth*sfi;
    kzp = kp*cth;
    
    //  Return back to the original coordinate system
    kx = kxp*cfi0*cth0-kyp*sfi0+kzp*cfi0*sth0;
    ky = kxp*sfi0*cth0+kyp*cfi0+kzp*sfi0*sth0;
    kz = -kxp*sth0+kzp*cth0;
    
    e = enew;
    
    return 0;
}

// Coulomb Angle BH //////////////////////////////

int Coulomb_angle_BH() {
    
    double cth0, sth0, cfi0, sfi0, sfi;
    double ge, rr, cth, sth, fi, cfi;
    
    //     Update carrier energy
    //     enew = e + w(i_fix,iv)
    
    //     Calculate the rotation angles
    kxy = sqrt(kx*kx+ky*ky);
    k = sqrt(kxy*kxy+kz*kz);
    cth0 = kz/k;
    sth0 = kxy/k;
    cfi0 = kx/kxy;
    sfi0 = ky/kxy;
    
    //     Randomize momentum in the rotated coordinate system
    ge = e*(1.+af[iv]*e);
    
    rr = (double)rand()/RAND_MAX;
    
    cth = 1. - 2*rr/(1.+4.*(1-rr)*ge/Energy_debye);
    sth = sqrt(1.-cth*cth);
    
    fi = two_pi*(double)rand()/RAND_MAX;
    
    cfi = cos(fi);
    sfi = sin(fi);
    kxp = k*sth*cfi;
    kyp = k*sth*sfi;
    kzp = k*cth;
    
    //     Return back to the original coordinate system
    kx = kxp*cfi0*cth0-kyp*sfi0+kzp*cfi0*sth0;
    ky = kxp*sfi0*cth0+kyp*cfi0+kzp*sfi0*sth0;
    kz = -kxp*sth0+kzp*cth0;
    
    //     e = enew
    
    return 0;
}

// Scatter Carrier //////////////////////////////

int scatter_carrier() {
    
    double rr, bound_lower, bound_upper;
    int i, i_top, i_fix, select_mech;
    unsigned int loc;
    
    //     Calculate index to the scattering table
    
    loc = (int)(e/de);
    if (loc == 0) { loc = 1; }
    if (loc > n_lev) { loc = n_lev; }
    
    
    // Select scattering mechanism
    
    i_top = max_scatt_mech[iv];
    rr = rand()/(double)RAND_MAX;
    
    if (rr >= scatt_table[loc][i_top][iv]) {
        freq[i_top+1][iv] = freq[i_top+1][iv] + 1;
        goto lbl_222;
    }
    
    if (rr < scatt_table[loc][1][iv]) {
        i_fix = 1;
        freq[i_fix][iv] = freq[i_fix][iv] + 1;
        goto lbl_111;
    }
    
    if (i_top > 1) {
        for (i = 1; i <= i_top-1; i++) {
            bound_lower = scatt_table[loc][i][iv];
            bound_upper = scatt_table[loc][i+1][iv];
            
            if ((rr >= bound_lower) && (rr < bound_upper)) {
                i_fix = i+1;
                freq[i_fix][iv] = freq[i_fix][iv] +1;
                goto lbl_111;
            }
            
        }
    }
    
lbl_111:
    
    //     Perform scattering (change energy and randomize momentum)
    
    select_mech = flag_mech[i_fix][iv];
    i_final = i_valley[i_fix][iv];
    
    if (select_mech == 1) {
        isotropic_g(i_fix); //isotropic(i_fix);
    } else if (select_mech == 2 ) {
        isotropic_f(i_fix); //polar_optical_angle(i_fix);
    } else if (select_mech == 3) {
        Coulomb_angle_BH();
    }
    
    iv = i_final;
    
lbl_222:
    
    return 0;
}

// Free Flight Scatter //////////////////////////////

int free_flight_scatter() {
    
    double rr;
    double dtau, dte, dte2, dt2, dt3, dtp;
    
    // Reset counter for scattering frequency
    
    int i;
    int j;
    
    for (i = 1; i <= 10; i++) {
        for (j = 1; j <= 3; j++) {
            freq[i][j] = 0.;
        }
    }
    
    for (i = 1; i <= nsim; i++) {
        
        
        // Inverse mapping of particle atributes
        
        kx = p[i][1];
        ky = p[i][2];
        kz = p[i][3];
        dtau = p[i][4];
        //        i_region = (int)p[i][8];
        iv = ip[i];
        e = energy[i];
        
        // Initial free-flight of the carriers
        
        dte = dtau;
        
        if (dte >= dt) {
            dt2 = dt;
        } else {
            dt2 = dte;
        }
        
        drift(dt2);
        
        if (dte > dt) { goto lbl_401; }
        
        // Free-flight and scatter part
        
    lbl_402:
        dte2 = dte;
        
        scatter_carrier();
        
    lbl_219:
        rr = (double)rand()/RAND_MAX;
        
        
        if (rr <= 1e-6) { goto lbl_219; }
        
        
        dt3 = -(log(rr))*tau_max[iv];
        dtp = dt - dte2;	// remaining time to scatter in dt-interval
        
        if (dt3 <= dtp) {
            dt2 = dt3;
        } else {
            dt2 = dtp;
        }
        
        drift(dt2);
        
        // Update times
        
        dte2 = dte2 + dt3;
        dte = dte2;
        
        if (dte < dt) { goto lbl_402; }
        
    lbl_401:
        dte = dte - dt;
        dtau = dte;
        
        // Map particle atributes
        
        p[i][1] = kx;
        p[i][2] = ky;
        p[i][3] = kz;
        p[i][4] = dtau;
        ip[i] = iv;
        energy[i] = e;
        //        p[i][8] = i_region;
    }
    
    return 0;
}

// Free Flight Scatter NEW //////////////////////////////

int free_flight_scatter_new() {
    
    double rr;
    double dtau, dte, dte2, dt2, dt3, dtp;
    
    // Reset counter for scattering frequency
    
    int i;
    //int j;
    
    /* for (i = 1; i <= 10; i++) {
     for (j = 1; j <= 3; j++) {
     freq[i][j] = 0.;
     }
     }*/
    
    for (i = 1; i <= nsim; i++) {
        
        
        // Inverse mapping of particle atributes
        
        kx = p[i][1];
        ky = p[i][2];
        kz = p[i][3];
        dtau = p[i][4];
        iv = ip[i];
        e = energy[i];
        
        // Initial free-flight of the carriers
        
        dte = dtau;
        
        if (dte >= dt) {
            dt2 = dt;
        } else {
            dt2 = dte;
        }
        
        drift(dt2);
        
        if (dte > dt) {
            goto lbl_401;
        }
        
        // Free-flight and scatter part
        
    lbl_402:
        dte2 = dte;
        
        scatter_carrier();
        
    lbl_219:
        rr = rand()/(double)RAND_MAX;
        
        
        if (rr <= 1e-6) {
            goto lbl_219;
        }
        
        
        dt3 = -(log(rr))*tau_max[iv];
        dtp = dt - dte2;	// remaining time to scatter in dt-interval
        
        if (dt3 <= dtp) {
            dt2 = dt3;
        } else {
            dt2 = dtp;
        }
        
        drift(dt2);
        
        // Update times
        
        dte2 = dte2 + dt3;
        dte = dte2;
        
        if (dte < dt) {
            goto lbl_402;
        }
        
    lbl_401:
        dte = dte - dt;
        dtau = dte;
        
        // Map particle atributes
        
        p[i][1] = kx;
        p[i][2] = ky;
        p[i][3] = kz;
        p[i][4] = dtau;
        ip[i] = iv;
        energy[i] = e;
    }
    
    return 0;
}

// Write //////////////////////////////

int write_m(int nsim, int iter, double time, bool flag_write) {
    int n_val = 3;
    
    double denom, velx, vely, velz;
    double nvaly[n_val], sume[n_val];
    double velx_sum[n_val], vely_sum[n_val], velz_sum[n_val];
    double velocity_x[n_val], velocity_y[n_val], velocity_z[n_val];
    
    int i;
    for (i = 1; i <= n_val; i++) {
        velx_sum[i] = 0.;
        vely_sum[i] = 0.;
        velz_sum[i] = 0.;
        sume[i] = 0.;
        nvaly[i] = 0;
    }
    
    for (i = 1; i <= nsim; i++) {
        iv = ip[i];
        ee = energy[i];
        denom = 1./(1.+af2[iv]*ee);
        velx = h*p[i][1]*denom/am[iv];
        vely = h*p[i][2]*denom/am[iv];
        velz = h*p[i][3]*denom/am[iv];
        velx_sum[iv] = velx_sum[iv] + velx;
        vely_sum[iv] = vely_sum[iv] + vely;
        velz_sum[iv] = velz_sum[iv] + velz;
        sume[iv] = sume[iv] + energy[i];
        nvaly[iv] = nvaly[iv] + 1;
        
    }
    
    for (i = 1; i <= n_val; i++) {
        if (nvaly[i] != 0) {
            velocity_x[i] = velx_sum[i]/nvaly[i];
            velocity_y[i] = vely_sum[i]/nvaly[i];
            velocity_z[i] = velz_sum[i]/nvaly[i];
            sume[i] = sume[i]/nvaly[i];
        }
    }
    
    if (flag_write == 0) {
        
        output1 << "time   vx_gamma    vx_L    vx_X" << std::endl;
        output2 << "time   vy_gamma    vy_L    vy_X" << std::endl;
        output3 << "time   vz_gamma    vz_L    vz_X" << std::endl;
        output4 << "time   Ek_gamma    Ek_L    Ek_X" << std::endl;
        output5 << "time   gamma    L_valley    X_valley" << std::endl;
        
    }
    
    if (iter % 4 == 0) {
        
        output1 << time << " " << velocity_x[1] << " " << velocity_x[2] << " " << velocity_x[3] << std::endl;
        output2 << time << " " << velocity_y[1] << " " << velocity_y[2] << " " << velocity_y[3] << std::endl;
        output3 << time << " " << velocity_z[1] << " " << velocity_z[2] << " " << velocity_z[3] << std::endl;
        output4 << time << " " << sume[1] << " " << sume[2] << " " << sume[3] << std::endl;
        output5 << time << " " << nvaly[1] << " " << nvaly[2] << " " << nvaly[3] << std::endl;
        
    }
    
    return 0;
    
}
